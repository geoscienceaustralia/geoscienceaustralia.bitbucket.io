# Geoscience Australia Bitbucket Web Pages

This repository publishes static websites on Bitbucket Cloud. Each subdirectory in the home
directory contains an index.html, other HTML documents, relavant files and diagrams. 

## Websites Published

* [GeodesyML Schema](https://geoscienceaustralia.bitbucket.io/geodesyml-schema) contains
HTML documentation with diagrams for GeodesyML and all supporting schemas. Geodesy Markup
Language (GeodesyML) is a standard way of describing (encoding) and sharing geodetic data and metadata.
Geoscience Australia (GA) is adopting GeodesyML as the standard for exchange of geodetic information.
For more information about GeodesyML, see http://www.geodesyml.org.


## Contact Information

Contributions, suggestions, and bug reports are welcome!

Please feel free to contact us through Bitbucket or directly via email at gnss@ga.gov.au.
